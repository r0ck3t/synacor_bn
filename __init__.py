from __future__ import print_function

import struct

from binaryninja import (LLIL_TEMP, Architecture, BranchType,
                         CallingConvention, FlagRole, InstructionInfo,
                         InstructionTextToken, InstructionTextTokenType,
                         LowLevelILFlagCondition, LowLevelILLabel, Type,
                         LowLevelILOperation, RegisterInfo, IntrinsicInfo, log_error)

MAX_INT = 32767
MATH_INT = 32768
REG_VAL = 32768

FIX_ADDRESSING = 2

Registers = [
    'r0',
    'r1',
    'r2',
    'r3',
    'r4',
    'r5',
    'r6',
    'r7',
    'pc',
    'sp'
]

NO_ARG_INSTR = [
    'halt',
    'ret',
    'noop'
]

ONE_ARG_INSTR = [
    'push',
    'pop',
    'jmp',
    'call',
    'out',
    'in'
]

TWO_ARG_INSTR = [
    'set',
    'jt',
    'jf',
    'not',
    'rmem',
    'wmem'
]

THREE_ARG_INSTR = [
    'eq',
    'gt',
    'add',
    'mult',
    'mod',
    'and',
    'or'
]


InstructionNames = [
    'halt',
    'set',
    'push',
    'pop',
    'eq',
    'gt',
    'jmp',
    'jt',
    'jf',
    'add',
    'mult',
    'mod',
    'and',
    'or',
    'not',
    'rmem',
    'wmem',
    'call',
    'ret',
    'out',
    'in',
    'noop'
]

REGISTER = 0
IMMEDIATE = 1
OUT = 2
JUMP = 3
CALL = 4

OperandTokens = [
    lambda reg, value: [ # Register
        InstructionTextToken(InstructionTextTokenType.RegisterToken, reg)
    ],
    lambda reg, value: [ # Immediate
        # I think this should just be an IntegerToken str(value), value
        InstructionTextToken(InstructionTextTokenType.PossibleAddressToken, hex(value), value)
    ],
    lambda reg, value: [ # Out
        InstructionTextToken(InstructionTextTokenType.CharacterConstantToken, chr(value), value)
    ],
    lambda reg, value: [ # Jump/JT/JF etc
        InstructionTextToken(InstructionTextTokenType.PossibleAddressToken, hex(value*2), value*2)
    ],
    # lambda reg, value: [ # Call
    #     InstructionTextToken(InstructionTextTokenType.CodeSymbolToken, hex(value*2), value*2) # TODO fix this so it displays the symbol
    # ]
]

def GetOperands(instr, a_val, b_val, c_val):
    if instr in NO_ARG_INSTR:
        return None, None, None, None, None, None
    elif instr in ONE_ARG_INSTR:
        if a_val > MAX_INT:
            a = Registers[a_val % REG_VAL]
            a_operand = REGISTER
        else:
            a = a_val
            a_operand = IMMEDIATE

            if instr == 'out':
                a_operand = OUT
            elif instr in ['jmp', 'call']:
                a_operand = JUMP
            # elif instr in ['call']:
            #     a_operand = CALL

        return a, a_operand, None, None, None, None
    elif instr in TWO_ARG_INSTR:
        if a_val > MAX_INT:
            a = Registers[a_val % REG_VAL]
            a_operand = REGISTER
        else:
            a = a_val
            a_operand = IMMEDIATE
        
        if b_val > MAX_INT:
            b = Registers[b_val % REG_VAL]
            b_operand = REGISTER
        else:
            b = b_val
            b_operand = IMMEDIATE

            if instr in ['jt', 'jf']:
                b_operand = JUMP

        return a, a_operand, b, b_operand, None, None
    elif instr in THREE_ARG_INSTR:
        if a_val > MAX_INT:
            a = Registers[a_val % REG_VAL]
            a_operand = REGISTER
        else:
            a = a_val
            a_operand = IMMEDIATE
        
        if b_val > MAX_INT:
            b = Registers[b_val % REG_VAL]
            b_operand = REGISTER
        else:
            b = b_val
            b_operand = IMMEDIATE

        if c_val > MAX_INT:
            c = Registers[c_val % REG_VAL]
            c_operand = REGISTER
        else:
            c = c_val
            c_operand = IMMEDIATE

        return a, a_operand, b, b_operand, c, c_operand
    else:
        log_error("Invalid instruction provided for GetOperands : {}".format(instr))
        return

def jump(il, dest):
    label = None

    if il[dest].operation == LowLevelILOperation.LLIL_CONST:
        label = il.get_label_for_address(
            Architecture['synacor'],
            il[dest].constant
        )
    
    if label is None:
        return il.jump(dest)
    else:
        return il.goto(label)

def call(il, a, a_op, a_val):
    if a_op == IMMEDIATE:
        call_expr = il.call(
            il.const_pointer(2, a_val * FIX_ADDRESSING)
        )
    else:
        load_expr = OperandsIL[a_op](il, 2, a, a_val)
        call_expr = il.call(
            il.const_pointer(2,
                il.mult(2,
                    il.reg(2, a),
                    il.const(2, FIX_ADDRESSING)
                )
            )
        )
        il.append(load_expr)

    il.append(call_expr)

OperandsIL = [
    lambda il, width, reg, value: [ # Register
        il.load(width, il.reg(2, reg)) #TODO
    ],
    lambda il, width, reg, value: [ # Immediate
        il.const(width, value) #TODO
    ],
    lambda il, width, reg, value: [ # Out
        il.unimplemented() #TODO
    ],
    lambda il, width, reg, value: [ # Jump/JT/JF etc
        il.unimplemented() #TODO
    ],
]


InstructionIL = {
    'halt': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.no_ret()
    ],
    'set': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.set_reg(2, a, il.reg(2, b)) if b_operand == REGISTER
        else il.set_reg(2, a, il.const(2, b_val))
    ],
    'push': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.push(2, OperandsIL[a_operand](il, width, a, a_val))
    ],
    'pop': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.set_reg(2, a, il.pop(2)) if a_operand == REGISTER
        else il.store(2, il.const_pointer(2, a), il.pop(2))
    ],
    'eq': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'gt': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'jmp': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        jump(il, a_val * FIX_ADDRESSING) #TODO
    ],
    'jt': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'jf': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'add': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'mult': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'mod': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'and': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'or': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'not': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'rmem': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'wmem': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'call': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.unimplemented() #TODO
    ],
    'ret': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.ret(il.pop(2))
    ],
    'out': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.intrinsic([], 'out', [il.reg(2, a)]) if a_operand == REGISTER
        else il.intrinsic([], 'out', [il.const(2, a_val)])
    ],
    'in': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.intrinsic([ILRegister(il.arch, LLIL_TEMP(il.temp_reg_count))], 'in', []) if a_operand == REGISTER
        else il.intrinsic([ILRegister(il.arch, LLIL_TEMP(il.temp_reg_count))], 'in', []),
        
        il.set_reg(2, a, il.reg(2, LLIL_TEMP(il.temp_reg_count)))
    ],
    'noop': lambda il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val: [
        il.nop()
    ]
}

class SYNACOR(Architecture):
    name = 'synacor'
    address_size = 2
    default_int_size = 2

    regs = {
        'r0': RegisterInfo('r0', 2),
        'r1': RegisterInfo('r1', 2),
        'r2': RegisterInfo('r2', 2),
        'r3': RegisterInfo('r3', 2),
        'r4': RegisterInfo('r4', 2),
        'r5': RegisterInfo('r5', 2),
        'r6': RegisterInfo('r6', 2),
        'r7': RegisterInfo('r7', 2),
        'pc': RegisterInfo('pc', 2),
        'sp': RegisterInfo('sp', 2),
    }

    # flags = []

    # flag_write_types = []

    # flags_written_by_flag_write_types = {}

    # flag_roles = {}

    # flag_required_for_flag_condition = {}

    stack_pointer = 'sp'

    intrinsics = {
        'out': IntrinsicInfo([Type.int(2)], []),
        'in': IntrinsicInfo([Type.int(2)], [Type.int(2)]),
    }

    def decode_instruction(self, data, addr):
        error_value = (None, None, None, None, None, None, None, None, None)
        if len(data) < 2:
            return error_value

        instruction = struct.unpack('<H', data[0:2])[0]

        opcode = instruction

        # print("Opcode is : {}".format(opcode))
        instr = InstructionNames[opcode]
        # print(instr, instruction)
        if instr is None:
            log_error('[{:x}] Bad opcode : {:x}'.format(addr, opcode))
            return error_value

        if instr in NO_ARG_INSTR:
            width = 0
        elif instr in ONE_ARG_INSTR:
            width = 1
        elif instr in TWO_ARG_INSTR:
            width = 2
        elif instr in THREE_ARG_INSTR:
            width = 3
        else:
            log_error('[{:x}] Instruction not found in argument list : {}'.format(addr, instr))
            return error_value

        length = 2 + (width * FIX_ADDRESSING)

        if len(data) < length:
            return error_value

        operand_length = length - 2

        if operand_length == 0:
            a_val, b_val, c_val = None, None, None
        elif operand_length == 2:
            a_val = struct.unpack('<H', data[2:4])[0]
            b_val, c_val = None, None
        elif operand_length == 4:
            a_val, b_val = struct.unpack('<HH', data[2:6])
            c_val = None
        elif operand_length == 6:
            a_val, b_val, c_val = struct.unpack('<HHH', data[2:8])
        else:
            log_error("Invalid operand length : {}".format(operand_length))

        a, a_operand, b, b_operand, c, c_operand = GetOperands(instr, a_val, b_val, c_val)


        return instr, width, a_operand, b_operand, c_operand, a, b, c, length, a_val, b_val, c_val

    def perform_get_instruction_info(self, data, addr):
        instr, _, a_operand, _, _, _, _, _, length, a_val, b_val, _ = self.decode_instruction(data, addr)

        if instr is None:
            return None

        result = InstructionInfo()
        result.length = length

        if instr in ['ret', 'halt']:
            result.add_branch(BranchType.FunctionReturn)
        elif instr in ['jmp']:
            result.add_branch(BranchType.UnconditionalBranch, a_val * FIX_ADDRESSING)
        elif instr in ['jt', 'jf']:
            if a_operand == IMMEDIATE:
                if instr == 'jt' and a_val == 0:
                    result.add_branch(BranchType.UnconditionalBranch, addr + length)
                elif instr == 'jt' and a_val != 0:
                    result.add_branch(BranchType.UnconditionalBranch, b_val * FIX_ADDRESSING)

                if instr == 'jf' and a_val == 0:
                    result.add_branch(BranchType.UnconditionalBranch, b_val * FIX_ADDRESSING)
                elif instr == 'jf' and a_val != 0:
                    result.add_branch(BranchType.UnconditionalBranch, addr + length)
            else:
                result.add_branch(BranchType.TrueBranch, b_val * FIX_ADDRESSING)
                result.add_branch(BranchType.FalseBranch, addr + length)
        elif instr in ['call']:
            result.add_branch(BranchType.CallDestination, a_val * FIX_ADDRESSING)

        return result

    def perform_get_instruction_text(self, data, addr):
        instr, _, a_operand, b_operand, c_operand, a, b, c, length, a_val, b_val, c_val = self.decode_instruction(data, addr)

        if instr is None:
            return None

        tokens = []

        instruction_text = instr

        tokens = [
            InstructionTextToken(InstructionTextTokenType.TextToken, '{:7s}'.format(instruction_text))
        ]

        if instr in ONE_ARG_INSTR:
            tokens += OperandTokens[a_operand](a, a_val)
        elif instr in TWO_ARG_INSTR:
            tokens += OperandTokens[a_operand](a, a_val)

            tokens += [InstructionTextToken(InstructionTextTokenType.TextToken, ', ')]

            tokens += OperandTokens[b_operand](b, b_val)
        elif instr in THREE_ARG_INSTR:
            tokens += OperandTokens[a_operand](a, a_val)

            tokens += [InstructionTextToken(InstructionTextTokenType.TextToken, ', ')]

            tokens += OperandTokens[b_operand](b, b_val)

            tokens += [InstructionTextToken(InstructionTextTokenType.TextToken, ', ')]

            tokens += OperandTokens[c_operand](c, c_val)

        return tokens, length

    def perform_get_instruction_low_level_il(self, data, addr, il):
        instr, width, a_operand, b_operand, c_operand, a, b, c, length, a_val, b_val, c_val = self.decode_instruction(data, addr)

        if instr is None:
            return None

        if InstructionIL.get(instr) is None:
            log_error('[0x{:4x}]: {} not implemented'.format(addr, instr))
            il.append(il.unimplemented())
        else:
            il_instr = InstructionIL[instr](
                il, width, a_operand, b_operand, c_operand, a, b, c, a_val, b_val, c_val
            )
            if isinstance(il_instr, list):
                for i in [i for i in il_instr if i is not None]:
                    il.append(i)
            elif il_instr is not None:
                il.append(il_instr)

        return length

SYNACOR.register()
